# Delete previous files
rm -f ./ceps.json
rm -f ./ceps.tar.gz

# Exports the collection to a json file
mongoexport --db cepgeo --collection ceps --out ./ceps.json

# Compresses it to a tar.gz file
tar -zcvf ./ceps.tar.gz ./ceps.json

# Sends it to the server - Password will be required
scp ceps.tar.gz marpontes@cep.tub.pe:/home/marpontes
